using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProAtividadeAPI.Models;

namespace ProAtividadeAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AtividadeController : ControllerBase
    {
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("GetId")]
        public ActionResult GetId(int id)
        {
            try 
            {
                Atividade GetActivities = new Atividade();
                int response = GetActivities.Id;
                return StatusCode(200, response);
            } 
            catch (Exception ex)
            {
                throw new Exception("Algo deu errado");
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpPost("PostId")]
        public ActionResult PostId(Atividade atividade)
        {
            try 
            {
                return StatusCode(200, atividade.Id);
            } 
            catch (Exception ex)
            {
                throw new Exception("Erro ao atualizar atividade");
            }
        }
    }
}